import React from 'react'

import './PrimaryNav.scss'

const menuItems = [
  {
    title: 'Products',
  },
  {
    title: 'Meet the Experts',
  },
  {
    title: 'Info Hub',
  },
  {
    title: 'Latest News',
  },
  {
    title: 'Branch Locator',
  },
]

const PrimaryNav = () => (
  <nav className="primary-nav">
    <ul>
      {menuItems.map(({ title }, index) => (
        <li key={index}>
          <a title={title} href="#">
            {title}
          </a>
        </li>
      ))}
    </ul>
  </nav>
)

export default PrimaryNav
