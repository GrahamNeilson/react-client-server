import React from 'react'

import './range.scss'

const Range = () => (
  <div className="range">
    <h2>Range</h2>
    <h3>Suspended Ceiling Grid</h3>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris venenatis, magna a tincidunt vulputate, orci risus
      venenatis dolor, ac sollicitudin leo purus a mi. Suspendisse euismod vitae diam quis malesuada. Class aptent
      taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis in iaculis nibh, auctor
      porttitor eros.
    </p>
    <div className="buttoncontainer">
      <button className="button" type="button">
        Discover more
      </button>
    </div>
  </div>
)

export default Range
