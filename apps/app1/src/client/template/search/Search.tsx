import React from 'react'

import './Search.scss'

const Search = () => <input type="text" placeholder="Search" />

export default Search
