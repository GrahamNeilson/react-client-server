import React from 'react'

import Menu from '../../styles/icons/Menu.svg'
import Account from '../../styles/icons/Account.svg'
import Search from '../../styles/icons/Search.svg'
import QuoteList from '../../styles/icons/QuoteList.svg'
import Logo from '../../assets/logo/ccf-logo.png'

import './head.scss'
import PrimaryNav from '../primary-nav'

const Head = () => (
  <div className="head">
    <header>
      <div className="menu-toggle-container">
        <a title="some title" href="#">
          <Menu className="icon" />
        </a>
      </div>
      <div className="logo-container">
        <a title="some title" href="#">
          <img src={Logo} className="image" alt="CCF Ltd" />
        </a>
      </div>
      <div className="header-actions-container">
        <a title="some title" href="#">
          <Search className="icon search" />
        </a>
        <a title="some title" href="#">
          <QuoteList className="icon quote-list" />
        </a>
        <a title="some title" href="#">
          <Account className="icon account" />
        </a>
      </div>
    </header>
    <div className="primary-nav-container">
      <PrimaryNav />
    </div>
  </div>
)

export default Head
