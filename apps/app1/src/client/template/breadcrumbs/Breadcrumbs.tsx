import React from 'react'

import Home from '../../styles/icons/Home.svg'
import './breadcrumbs.scss'

const Breadcrumbs = () => (
  <nav className="breadcrumbs">
    <a title="some title" href="#" style={{ paddingTop: '7px' }}>
      <Home />
    </a>{' '}
    <span>&gt;</span>{' '}
    <a title="some title" href="#">
      Current Page
    </a>
  </nav>
)

export default Breadcrumbs
