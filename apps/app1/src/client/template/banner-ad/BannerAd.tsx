import React from 'react'

import Banner from '../../../client/images/ceilings-1200.jpeg'
import './banner-ad.scss'

const BannerAd = () => (
  <div className="banner-ad">
    <img alt="some alt text" src={Banner} width="1200" height="468" />
  </div>
)

export default BannerAd
