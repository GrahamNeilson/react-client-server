import React from 'react'

import './footer.scss'

const Footer = () => (
  <footer className="footer">
    <div className="subscription-block">Subscription / Contact Form / Sales block</div>
    <div className="main">
      <nav className="menu">
        <h3>Products</h3>
        <ul className="list">
          <li>
            <a href="#">Ceilings</a>
          </li>
          <li>
            <a href="#">Drywall</a>
          </li>
          <li>
            <a href="#">Fire Proection</a>
          </li>
          <li>
            <a href="#">Flooring</a>
          </li>
          <li>
            <a href="#">Insulation</a>
          </li>
        </ul>
      </nav>
      <nav className="menu">
        <h3>Meet the Experts</h3>
        <ul className="list">
          <li>
            <a href="#">Branch Teams</a>
          </li>
          <li>
            <a href="#">Technical Team</a>
          </li>
          <li>
            <a href="#">Business Development Team</a>
          </li>
          <li>
            <a href="#">National Sales Team</a>
          </li>
          <li>
            <a href="#">Framework Team</a>
          </li>
        </ul>
      </nav>
      <nav className="menu">
        <h3>Info Hub</h3>
        <ul className="list">
          <li>
            <a href="#">Open Account</a>
          </li>
          <li>
            <a href="#">FAQs</a>
          </li>
          <li>
            <a href="#">Branch Finder</a>
          </li>
          <li>
            <a href="#">Brochures</a>
          </li>
          <li>
            <a href="#">Contact Us</a>
          </li>
        </ul>
      </nav>
      <nav className="menu">
        <h3>Branch Locator</h3>
      </nav>
      <nav className="menu">
        <h3>Blog</h3>
        <ul className="list">
          <li>
            <a href="#">Open Account</a>
          </li>
          <li>
            <a href="#">FAQs</a>
          </li>
          <li>
            <a href="#">Branch Finder</a>
          </li>
          <li>
            <a href="#">Brochures</a>
          </li>
          <li>
            <a href="#">Contact Us</a>
          </li>
        </ul>
      </nav>
    </div>
    <div className="alt">
      <div>
        <div>CCF - Nationwide Distributor of Interior building products</div>
        <address>Lodge Way House, Lodgeway, Harlestone Road, Northampton NN5 7UG</address>
        <br />
        Copyright © 2022 CCF | Privacy Policy | Terms & conditions
        <br />
      </div>

      <div>
        Registered in England No: 00824821
        <br />
        VAT registration number: 408556737
      </div>
    </div>
  </footer>
)

export default Footer
