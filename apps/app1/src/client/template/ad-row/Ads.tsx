import React from 'react'

import Ad1 from '../../../client/images/ad1.jpeg'
import Ad2 from '../../../client/images/ad2.jpeg'
import Ad3 from '../../../client/images/ad3.jpeg'

import './ads.scss'

const Ads = () => (
  <div className="ads">
    <h2>Some Other Title</h2>
    <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr 1fr', columnGap: '24px' }}>
      <img alt="some alt text" src={Ad1} />
      <img alt="some alt text" src={Ad2} />
      <img alt="some alt text" src={Ad3} />
    </div>
  </div>
)

export default Ads
