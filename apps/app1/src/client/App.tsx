import React from 'react'

import Head from './template/head'
import Footer from './template/footer'
import Breadcrumbs from './template/breadcrumbs'
import BannerAd from './template/banner-ad'
import Range from './template/range'
import Ads from './template/ad-row/Ads'

import './app.scss'

const App = () => (
  <main>
    <Head />
    <article data-page-type="home">
      <div className="breadcrumbs-container">
        <Breadcrumbs />
      </div>
      {/* <section>
        <BannerAd />
      </section>
      <section>
        <Range />
      </section>
      <section>
        <Ads />
      </section>
      <section>
        <h2>Article</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris venenatis, magna a tincidunt vulputate, orci
          risus venenatis dolor,{' '}
          <a title="ac sollicitudin leo purus a mi" href="#" className="link">
            ac sollicitudin leo purus a mi
          </a>
          . Suspendisse euismod vitae diam quis malesuada. Class aptent taciti sociosqu ad litora torquent per conubia
          nostra, per inceptos himenaeos. Duis in iaculis nibh, auctor porttitor eros.
        </p>
        <p>
          Proin semper et tellus in iaculis. Fusce non lorem ultrices, facilisis tellus a, tristique purus. Pellentesque
          habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris blandit nibh id erat
          facilisis, sed aliquam magna dapibus. Quisque ultrices sollicitudin congue. Mauris eu dolor diam. Morbi auctor
          maximus pulvinar. Integer malesuada faucibus felis, aliquam rhoncus nulla semper eget.
        </p>
      </section> */}
    </article>
    <Footer />
  </main>
)

export default App
