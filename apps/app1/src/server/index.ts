import * as http from 'http'
import app from './app'
import dotenv from 'dotenv'

dotenv.config()

const { PORT = 8080 } = process.env

const server = http.createServer(app).listen(PORT, () => {
  // eslint-disable-next-line no-console
  console.log(`Application server is running on http://localhost:${PORT}`)
})

export default server
