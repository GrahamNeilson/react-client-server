import express, { Request, Response } from 'express'
import * as path from 'path'

const app = express()

// TODO: document why exactly is this required (and others)
app.use(express.static('./dist/client'))

app.get('/*', (_: Request, res: Response) => {
  const filePath = path.join(__dirname, '../../dist/client', 'index.html')

  res.sendFile(filePath)
})

export default app
