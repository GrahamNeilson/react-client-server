const { merge } = require('webpack-merge')
const dotenv = require('dotenv')

const commonConfig = require('./webpack.common')
const developmentConfig = require('./webpack.development')
const productionConfig = require('./webpack.production')

dotenv.config()

module.exports = (env, argv) => {
  const { mode } = argv

  switch (mode) {
    case 'development':
      return merge(commonConfig(env, argv), developmentConfig(env, argv))
    case 'production':
      return merge(commonConfig(env, argv), productionConfig(env, argv))
    default:
      throw new Error('No matching configuration was found!')
  }
}
