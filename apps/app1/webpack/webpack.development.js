const path = require('path')

module.exports = (env, { mode }) => {
  return {
    devServer: {
      compress: true,
      static: {
        directory: path.join(__dirname, 'dist/client'),
      },
      port: process.env.PORT,
    },
  }
}
