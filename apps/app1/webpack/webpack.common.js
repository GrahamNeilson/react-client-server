const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = (env, args) => {
  const { mode } = args
  return {
    entry: './src/client/index.tsx',
    mode,
    output: {
      filename: 'react_ui/scripts/[name].[chunkhash].js',
      path: path.resolve(__dirname, '../', 'dist/client'),
      clean: true,
    },
    module: {
      rules: [
        {
          test: /\.tsx|ts?$/,
          use: [
            {
              loader: 'ts-loader',
              options: {
                configFile: 'tsconfig.client.json',
              },
            },
          ],
          exclude: /node_modules/,
        },
        {
          test: /\.scss$/i,
          use: ['style-loader', 'css-loader', 'sass-loader'],
        },
        {
          test: /\.png|gif|jpg|jpeg$/i,
          use: ['file-loader'],
        },
        {
          test: /\.svg$/,
          use: ['@svgr/webpack'],
        },
      ],
    },
    resolve: {
      alias: {
        src: path.resolve('.', 'src/**'),
      },
      extensions: ['.js', '.mjs', '.ts', '.tsx', '.json'],
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: './public/index.html',
      }),
    ],
  }
}
